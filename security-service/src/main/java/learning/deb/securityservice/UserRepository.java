package learning.deb.securityservice;

import org.springframework.data.jpa.repository.JpaRepository;
import java.lang.Long;
import learning.deb.securityservice.User;


public interface UserRepository extends JpaRepository<User, Long> {
	User findOneByUsername(String username);
}