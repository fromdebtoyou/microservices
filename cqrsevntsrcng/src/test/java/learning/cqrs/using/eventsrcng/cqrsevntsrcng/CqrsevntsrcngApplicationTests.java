package learning.cqrs.using.eventsrcng.cqrsevntsrcng;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Before;
import learning.cqrs.using.eventsrcng.cqrsevntsrcng.account.Account;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import learning.cqrs.using.eventsrcng.cqrsevntsrcng.coreapi.CreateAccountCommand;
import learning.cqrs.using.eventsrcng.cqrsevntsrcng.coreapi.WithdrawMoneyCommand;
import learning.cqrs.using.eventsrcng.cqrsevntsrcng.coreapi.AccountWithdrawnEvent;

public class CqrsevntsrcngApplicationTests {

	private FixtureConfiguration<Account> fixture;

	@Before
	public void setUp() throws Exception{
		fixture = new AggregateTestFixture<Account>(Account.class);
		/*fixture.registerAnnotatedCommandHandler(new FootBallService());
		fixture.newGivenWhenThenFixture(Account.class);*/
	}

	@Test
	public void testCreation() throws Exception {

		fixture.givenCommands(new CreateAccountCommand("123545",600,300))
						.when(new WithdrawMoneyCommand("123545",300))
						.expectEvents(new AccountWithdrawnEvent("123545",300));

		}


}
