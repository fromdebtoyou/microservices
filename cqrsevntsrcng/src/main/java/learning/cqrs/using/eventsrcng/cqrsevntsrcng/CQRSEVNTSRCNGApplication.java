package learning.cqrs.using.eventsrcng.cqrsevntsrcng;

import learning.cqrs.using.eventsrcng.cqrsevntsrcng.coreapi.CreateAccountCommand;
import learning.cqrs.using.eventsrcng.cqrsevntsrcng.coreapi.WithdrawMoneyCommand;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.spring.config.EnableAxon;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import static org.axonframework.commandhandling.GenericCommandMessage.asCommandMessage;


@SpringBootApplication
@EnableAxon
//@ComponentScan({"learning.cqrs.using.eventsrcng.cqrsevntsrcng"})
@EnableJpaRepositories("learning.cqrs.using.eventsrcng.cqrsevntsrcng.account")
@EntityScan(basePackages = {"learning.cqrs.using.eventsrcng.cqrsevntsrcng.account",
		"org.axonframework.eventsourcing.eventstore.jpa",
		"org.axonframework.eventhandling.tokenstore.jpa"})
public class CQRSEVNTSRCNGApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext config =  SpringApplication.run(CQRSEVNTSRCNGApplication.class, args);

		CommandBus commandBus = config.getBeanFactory().getBean(CommandBus.class);

		commandBus.dispatch(asCommandMessage(new CreateAccountCommand("123545",600,300)));
		commandBus.dispatch(asCommandMessage(new WithdrawMoneyCommand("123545",300)));

	}

	/*@Bean
	public Repository<Account> accountRepository(EntityManagerProvider entityManagerProvider, EventBus eventBus)
	{
		return new GenericJpaRepository<>(entityManagerProvider, Account.class, eventBus);
	}*/


}
