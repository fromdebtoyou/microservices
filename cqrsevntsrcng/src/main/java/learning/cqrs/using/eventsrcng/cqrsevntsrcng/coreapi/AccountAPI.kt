package learning.cqrs.using.eventsrcng.cqrsevntsrcng.coreapi

import org.axonframework.commandhandling.TargetAggregateIdentifier

/**
 * Created by 114222 on 7/12/2017.
 */
class CreateAccountCommand(val accountid: String, val deposit: Int, val overdrawnlimit: Int)
class WithdrawMoneyCommand(@TargetAggregateIdentifier val accountid: String, val amount: Int)

class AccountCreatedEvent(val accountid:String,val amount: Int)
class AccountWithdrawnEvent(val accountid:String, val deposit:Int )