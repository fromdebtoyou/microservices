package learning.cqrs.using.eventsrcng.cqrsevntsrcng.config;

import learning.cqrs.using.eventsrcng.cqrsevntsrcng.account.Account;
import org.axonframework.commandhandling.model.GenericJpaRepository;
import org.axonframework.commandhandling.model.Repository;
import org.axonframework.common.jpa.ContainerManagedEntityManagerProvider;
import org.axonframework.common.jpa.EntityManagerProvider;
import org.axonframework.common.jpa.SimpleEntityManagerProvider;
import org.axonframework.common.transaction.TransactionManager;
import org.axonframework.config.EventHandlingConfiguration;
import org.axonframework.eventhandling.EventBus;
import org.axonframework.eventsourcing.EventSourcingRepository;
import org.axonframework.eventsourcing.eventstore.EmbeddedEventStore;
import org.axonframework.eventsourcing.eventstore.jpa.DomainEventEntry;
import org.axonframework.eventsourcing.eventstore.jpa.JpaEventStorageEngine;
import org.axonframework.spring.messaging.unitofwork.SpringTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by 114222 on 7/17/2017.
 */

@Configuration
@Component
public class AxonConfig {

    // Below is sample code on how you would register your own SagaStore, JpaStore... etc
    // @Autowired
    // private SagaStore<Object> sagaStore;
    //
     @Autowired
     private TransactionManager transactionManager;

     @PersistenceContext
     private EntityManager entityManager;
    //
    // @Bean
    // public SagaStore<Object> sagaStore() {
    // return new JpaSagaStore(entityManagerProvider());
    // }
    //
    // @Bean
    // public SpringResourceInjector resourceInjector() {
    // return new SpringResourceInjector();
    // }
    //
    // @Bean
    // public SagaRepository<MoneyTransferSaga> moneyTransferSagaRepository() {
    // return new AnnotatedSagaRepository<>(MoneyTransferSaga.class, sagaStore, resourceInjector());
    // }
    //
    // @Bean
    // public AbstractSagaManager<MoneyTransferSaga> moneyTransferSagaManager() {
    // return new AnnotatedSagaManager<>(
    // MoneyTransferSaga.class,
    // moneyTransferSagaRepository());
    // }
    //
    @Bean
    public JpaEventStorageEngine eventStorageEngine() {
        ContainerManagedEntityManagerProvider ctnmp = new ContainerManagedEntityManagerProvider();
        ctnmp.setEntityManager(entityManager);
     return new JpaEventStorageEngine(ctnmp, transactionManager);
    }
    //
    @Bean
    public Repository<Account> jpaAccountRepository(EventBus eventBus) {
     return new GenericJpaRepository<>(entityManagerProvider(), Account.class, eventBus);
    }

   /* @Bean
    public Repository<Account> jpaAccountRepository() {
        return new EventSourcingRepository<Account>(Account.class, new EmbeddedEventStore(eventStorageEngine()));
    }*/


    //
    // @Bean
    // public Repository<MoneyTransfer> jpaMoneyTransferRepository(EventBus eventBus) {
    // return new GenericJpaRepository<>(entityManagerProvider(), MoneyTransfer.class, eventBus);
    // }
    //
    @Bean
    public EntityManagerProvider entityManagerProvider() {
      return new ContainerManagedEntityManagerProvider();
    }
    //
    @Bean
    public TransactionManager axonTransactionManager(PlatformTransactionManager platformTransactionManager) {
     return new SpringTransactionManager(platformTransactionManager);
     }

    // @Autowired
    // public void configure(EventHandlingConfiguration configuration) {
    // configuration.registerTrackingProcessor("TransactionHistory");
    // }
}
