package learning.cqrs.using.eventsrcng.cqrsevntsrcng;

import learning.cqrs.using.eventsrcng.cqrsevntsrcng.account.Account;
import learning.cqrs.using.eventsrcng.cqrsevntsrcng.coreapi.CreateAccountCommand;
import learning.cqrs.using.eventsrcng.cqrsevntsrcng.coreapi.WithdrawMoneyCommand;
import org.axonframework.commandhandling.AsynchronousCommandBus;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.common.jpa.ContainerManagedEntityManagerProvider;
import org.axonframework.common.jpa.EntityManagerProvider;
import org.axonframework.config.Configuration;
import org.axonframework.config.DefaultConfigurer;
import org.axonframework.eventsourcing.eventstore.inmemory.InMemoryEventStorageEngine;
import org.axonframework.eventsourcing.eventstore.jpa.JpaEventStorageEngine;
import org.axonframework.spring.messaging.unitofwork.SpringTransactionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.metamodel.Metamodel;

import java.util.List;
import java.util.Map;

import static org.axonframework.commandhandling.GenericCommandMessage.asCommandMessage;

/**
 * Created by 114222 on 7/13/2017.
 */
public class CQRSApplication {

    public static void main(String[] args){
        Configuration config = DefaultConfigurer
                                    .defaultConfiguration()
                                    .configureAggregate(Account.class)
                                    .configureEmbeddedEventStore(c -> new InMemoryEventStorageEngine())
                                    //.configureCommandBus(c -> new AsynchronousCommandBus())
                                    .buildConfiguration();
        config.start();

        CommandBus objCommmandBus = config.commandBus();
        objCommmandBus.dispatch(asCommandMessage(new CreateAccountCommand("123545",600,300)));
        objCommmandBus.dispatch(asCommandMessage(new WithdrawMoneyCommand("123545",300)));
    }



}
