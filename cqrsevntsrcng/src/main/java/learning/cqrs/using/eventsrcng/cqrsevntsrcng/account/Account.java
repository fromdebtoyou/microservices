package learning.cqrs.using.eventsrcng.cqrsevntsrcng.account;

import learning.cqrs.using.eventsrcng.cqrsevntsrcng.coreapi.AccountCreatedEvent;
import learning.cqrs.using.eventsrcng.cqrsevntsrcng.coreapi.AccountWithdrawnEvent;
import learning.cqrs.using.eventsrcng.cqrsevntsrcng.coreapi.CreateAccountCommand;
import learning.cqrs.using.eventsrcng.cqrsevntsrcng.coreapi.WithdrawMoneyCommand;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;
import lombok.AllArgsConstructor;
import lombok.Data;


/**
 * Created by 114222 on 7/12/2017.
 */
//@Aggregate
@Aggregate(repository = "jpaAccountRepository")
@NoArgsConstructor
@Entity
@Data
@AllArgsConstructor
public class Account {

    @AggregateIdentifier
    @Id
    private String accountid;

    @Basic
    private Integer deposit;

    @CommandHandler
    public Account(CreateAccountCommand command) {
        apply(new AccountCreatedEvent(command.getAccountid(),command.getDeposit()));
    }

    @CommandHandler
    public void withdrawMoney(WithdrawMoneyCommand command) {
        apply(new AccountWithdrawnEvent(command.getAccountid(),command.getAmount()));
    }

    @EventSourcingHandler
    public void on(AccountCreatedEvent event){

        this.accountid = event.getAccountid();
        this.deposit = event.getAmount();
    }

    @EventSourcingHandler
    public void on(AccountWithdrawnEvent event){
        this.deposit -= event.getDeposit();
    }
}
