package learn.deb.ms.booksmanagement;

import learn.deb.ms.booksmanagement.coreapi.UpdateBookMetaCommand;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import learn.deb.ms.booksmanagement.commandhandler.BookMetaCommandHandler;
import learn.deb.ms.booksmanagement.coreapi.BookMetaCreatedEvent;
import learn.deb.ms.booksmanagement.coreapi.CreateBookMetaCommand;
import learn.deb.ms.booksmanagement.entities.BookMeta;


//@RunWith(SpringRunner.class)
@SpringBootTest
@EntityScan(basePackages = {"learn.deb.ms.booksmanagement.entities",
		"learn.deb.ms.booksmanagement.commandhandler",
		"learn.deb.ms.booksmanagement.config",
		"org.axonframework.eventsourcing.eventstore.jpa",
		"org.axonframework.eventhandling.tokenstore.jpa"})
@ComponentScan(basePackages = {
		"learn.deb.ms.booksmanagement.commandhandler" },
basePackageClasses = BookMetaCommandHandler.class)
public class BooksmanagementApplicationTests {

	private FixtureConfiguration<BookMeta> fixture;

	@Before
	public void setUp() throws Exception{
		fixture = new AggregateTestFixture<BookMeta>(BookMeta.class);
		fixture.registerAnnotatedCommandHandler(new BookMetaCommandHandler());
		//fixture.newGivenWhenThenFixture(Account.class);
	}
	
	@Test
	public void testCreation() throws Exception {

		fixture.givenCommands(new CreateBookMetaCommand("123547","Chika","comedy","horror"))
				.when(new UpdateBookMetaCommand("123547","Chika","comedy","horror"))
						.expectEvents(new BookMetaCreatedEvent("123547","Chika","comedy","horror"));

		}

}
