package learn.deb.ms.booksmanagement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import learn.deb.ms.booksmanagement.entities.BorrowStatus;

public interface BorrowStatusRepository extends JpaRepository<BorrowStatus,String>{
	
}
