package learn.deb.ms.booksmanagement.model;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

import javax.persistence.*;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.commandhandling.model.AggregateRoot;
import org.axonframework.commandhandling.model.Repository;
import org.axonframework.eventhandling.EventBus;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import learn.deb.ms.booksmanagement.coreapi.BookCreatedEvent;
import learn.deb.ms.booksmanagement.coreapi.BookMetaCreatedEvent;
import learn.deb.ms.booksmanagement.coreapi.CreateBookCommand;
import learn.deb.ms.booksmanagement.coreapi.CreateBookMetaCommand;
import learn.deb.ms.booksmanagement.entities.BookLocation;
import learn.deb.ms.booksmanagement.entities.BookMeta;
import learn.deb.ms.booksmanagement.entities.BorrowStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AggregateRoot
@Aggregate(repository = "libraryBook")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name="books")
public class Book {
	
	@AggregateIdentifier
	@Id
	private String bookid;
	
	@Basic
	private int price;
	
	@Basic
	private String yrofpublication;
	
	@Basic
	private int status;
	
	@Basic 
	private int metaid;
	
	@Transient
	private BookLocation bookLocation;

	@Transient
	private BookMeta bookMeta;

	@Transient
	private BorrowStatus borrowStatus;
	
	/*@CommandHandler
	public void createBook(CreateBookCommand command){
		apply(new BookCreatedEvent(command.getName(),command.getAuthor(),command.getGenre()));
	}
	
	@EventSourcingHandler
	public void on(BookCreatedEvent event){
		BookMeta objMeta = new BookMeta(event.getName(),event.getAuthor(),event.getGenre());
	} */
	
	
}
