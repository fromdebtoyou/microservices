package learn.deb.ms.booksmanagement;

import org.axonframework.commandhandling.CommandBus;
import org.axonframework.spring.config.EnableAxon;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.archaius.ConfigurableEnvironmentConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import static org.axonframework.commandhandling.GenericCommandMessage.asCommandMessage;

import learn.deb.ms.booksmanagement.commandhandler.BookMetaCommandHandler;
import learn.deb.ms.booksmanagement.coreapi.CreateBookMetaCommand;
import learn.deb.ms.booksmanagement.coreapi.UpdateBookMetaCommand;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("learn.deb.ms.booksmanagement.repositories")
@EntityScan(basePackages = {"learn.deb.ms.booksmanagement.entities","learn.deb.ms.booksmanagement.model","learn.deb.ms.booksmanagement.commandhandler",
		"learn.deb.ms.booksmanagement.config","org.axonframework.eventsourcing.eventstore.jpa","org.axonframework.eventhandling.saga.repository.jpa",
		"org.axonframework.eventhandling.tokenstore.jpa"})
@ComponentScan(basePackages = {"learn.deb.ms.booksmanagement.commandhandler" },basePackageClasses = BookMetaCommandHandler.class)

public class BooksmanagementApplication {

	public static void main(String[] args) {
		System.setProperty("spring.devtools.restart.enabled", "false");
		
		ConfigurableApplicationContext context = SpringApplication.run(BooksmanagementApplication.class, args);
				
		CommandBus commandBus = context.getBeanFactory().getBean(CommandBus.class);

		commandBus.dispatch(asCommandMessage(new CreateBookMetaCommand("123555","R N T","hati","horror")));
		commandBus.dispatch(asCommandMessage(new UpdateBookMetaCommand("123555","N T R","hati","comedy")));
		commandBus.dispatch(asCommandMessage(new UpdateBookMetaCommand("123555","N M R","hati","comedy")));
		commandBus.dispatch(asCommandMessage(new UpdateBookMetaCommand("123555","N K R","hati","comedy")));
	}
}
