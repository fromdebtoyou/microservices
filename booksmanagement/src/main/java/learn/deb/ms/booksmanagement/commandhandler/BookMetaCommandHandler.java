package learn.deb.ms.booksmanagement.commandhandler;

import learn.deb.ms.booksmanagement.coreapi.BookMetaUpdatedEvent;
import learn.deb.ms.booksmanagement.coreapi.UpdateBookMetaCommand;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.Aggregate;
import org.axonframework.commandhandling.model.Repository;
import org.axonframework.common.transaction.TransactionManager;
import org.axonframework.eventhandling.EventBus;
import org.axonframework.eventhandling.EventMessage;
import org.axonframework.eventsourcing.AggregateFactory;
import org.axonframework.eventsourcing.EventSourcingRepository;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.axonframework.messaging.interceptors.TransactionManagingInterceptor;
import org.axonframework.spring.eventsourcing.SpringPrototypeAggregateFactory;
import org.hibernate.event.spi.AbstractEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import learn.deb.ms.booksmanagement.config.AxonConfig;
import learn.deb.ms.booksmanagement.coreapi.BookMetaCreatedEvent;
import learn.deb.ms.booksmanagement.coreapi.CreateBookMetaCommand;
import learn.deb.ms.booksmanagement.entities.BookMeta;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

@Component
@ComponentScan(basePackages = {
"learn.deb.ms.booksmanagement.config" },
basePackageClasses = AxonConfig.class)
public class BookMetaCommandHandler {
	
	    @Autowired
		EventBus eventBus;
	    //Comment this for JpaRepo


	   	private Repository<BookMeta> repository;

		@Autowired
		@Qualifier("bmRepository")
		public void setRepository(Repository<BookMeta> repository) {
			this.repository = repository;
		}
		
		@CommandHandler
		public void createBookMeta(CreateBookMetaCommand command) throws Exception{
				Aggregate<BookMeta> aggrBM = repository.newInstance(() -> {
					return new BookMeta(command.getMetaid(),command.getName(),command.getAuthor(),command.getGenre());
				});
				//System.out.println(aggrBM.identifierAsString());
				apply(new BookMetaCreatedEvent(command.getMetaid(),command.getName(),command.getAuthor(),command.getGenre()));

		}

		@CommandHandler
		public void updateBookMeta(UpdateBookMetaCommand command) throws Exception{
				Aggregate<BookMeta> aggrBM = repository.load(command.getMetaid(),null);

				aggrBM.execute(bookMeta -> {
					bookMeta.setName(command.getName());
					bookMeta.setAuthor(command.getAuthor());
				});

				apply(new BookMetaUpdatedEvent(command.getMetaid(),command.getName(),command.getAuthor(),command.getGenre()));

		}
		


}
