package learn.deb.ms.booksmanagement.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="book_location")
@Getter
@Setter
@NoArgsConstructor
public class BookLocation {

	@Id
	private String booklocationid;

	@Basic
	private String shelfno;
	
	@Basic
	private String rowno;
	
	@Basic
	private int bookid;
	
}
