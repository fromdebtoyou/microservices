package learn.deb.ms.booksmanagement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import learn.deb.ms.booksmanagement.model.Book;

public interface BookRepository extends JpaRepository<Book, String>{

}
