package learn.deb.ms.booksmanagement.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name="book_borrower")
@Getter
@Setter
@NoArgsConstructor
public class BorrowStatus {
	
	@Id
	private String idbookborrower;
	
	@Basic
	private int bookid;
	
	@Basic
	private int borrowerid;
	
	@Basic
	private int isreturned;
	
	@Basic
	private String borrowedon;
	
	
	@Basic
	private String returnedon;
	
	@Basic
	private int timesrenewed;
}
