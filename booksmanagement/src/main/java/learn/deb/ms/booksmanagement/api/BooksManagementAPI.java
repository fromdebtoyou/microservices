package learn.deb.ms.booksmanagement.api;

import java.util.List;

import learn.deb.ms.booksmanagement.entities.BookLocation;
import learn.deb.ms.booksmanagement.entities.BookMeta;
import learn.deb.ms.booksmanagement.entities.BorrowStatus;
import learn.deb.ms.booksmanagement.model.Book;

public interface BooksManagementAPI {
	
	//Command side
	public Book createBook(Book book) throws Exception;
	public BookMeta createBookMeta(BookMeta objMeta) throws Exception;
	public BookMeta updateBookMeta(BookMeta objMeta) throws Exception;
	public boolean removeBookMeta(int bookmetaId) throws Exception;
	public BookLocation updateBookLocation(BookLocation objLocation) throws Exception;
	public BorrowStatus updateBorrowStatus(BorrowStatus objBorrowStatus) throws Exception;
	
	//Query side
	public List<Book> findBook(BookMeta objMeta) throws Exception;
	public BorrowStatus checkBookStatus(int bookId) throws Exception;



}
