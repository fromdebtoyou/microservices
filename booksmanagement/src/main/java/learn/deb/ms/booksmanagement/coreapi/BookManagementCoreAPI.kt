package learn.deb.ms.booksmanagement.coreapi

import org.axonframework.commandhandling.TargetAggregateIdentifier

class CreateBookMetaCommand(val metaid: String, val name: String, val author: String, val genre: String)
class BookMetaCreatedEvent(val metaid: String,val name: String, val author: String, val genre: String)

class UpdateBookMetaCommand(@TargetAggregateIdentifier val metaid: String, val name: String, val author: String, val genre: String)
class BookMetaUpdatedEvent(val metaid: String,val name: String, val author: String, val genre: String)


class CreateBookCommand(val metaid: Int, val price: Int, val yrofpublication: String)
class BookCreatedEvent(val metaid: Int, val price: Int, val yrofpublication: String)

class RemoveBookCommand(val bookid: Int)
class BookRemovedEvent(val bookid: Int)

class UpdateBookLocationCommand(@TargetAggregateIdentifier val bookid: Int, val shelfno: String, val rowno: String)
class BookLocationUpdatedEvent(val bookid: Int, val shelfno: String, val rowno: String)

class UpdateBorrowStatusCommand(@TargetAggregateIdentifier val bookid: Int, val borrowerid: Int, val isreturned: Int)
class BorrowStatusUpdatedEvent(val bookid: Int, val borrowerid: Int, val isreturned: Int)