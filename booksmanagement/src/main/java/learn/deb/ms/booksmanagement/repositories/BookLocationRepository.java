package learn.deb.ms.booksmanagement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import learn.deb.ms.booksmanagement.entities.BookLocation;

public interface BookLocationRepository extends JpaRepository<BookLocation,String>{

}
