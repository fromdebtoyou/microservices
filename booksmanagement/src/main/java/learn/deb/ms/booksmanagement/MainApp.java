package learn.deb.ms.booksmanagement;

import org.axonframework.commandhandling.CommandBus;
import org.axonframework.config.Configuration;
import org.axonframework.config.DefaultConfigurer;
import org.axonframework.eventsourcing.eventstore.inmemory.InMemoryEventStorageEngine;
import org.axonframework.spring.config.EnableAxon;

import learn.deb.ms.booksmanagement.commandhandler.BookMetaCommandHandler;
import learn.deb.ms.booksmanagement.coreapi.CreateBookMetaCommand;
import learn.deb.ms.booksmanagement.entities.BookMeta;
import static org.axonframework.commandhandling.GenericCommandMessage.asCommandMessage;

@EnableAxon
public class MainApp {

	public static void main(String[] args) {
		Configuration config = DefaultConfigurer
                .defaultConfiguration()
                .configureAggregate(BookMeta.class)
                .registerCommandHandler(c -> new BookMetaCommandHandler())
                .configureEmbeddedEventStore(c -> new InMemoryEventStorageEngine())
                //.configureCommandBus(c -> new AsynchronousCommandBus())
                .buildConfiguration();
		config.start();
		
		CommandBus objCommmandBus = config.commandBus();
		objCommmandBus.dispatch(asCommandMessage(new CreateBookMetaCommand("123545","R N T","hati","horror")));

	}

}
