package learn.deb.ms.booksmanagement.config;

import org.axonframework.commandhandling.model.GenericJpaRepository;
import org.axonframework.commandhandling.model.Repository;
import org.axonframework.common.caching.Cache;
import org.axonframework.common.caching.WeakReferenceCache;
import org.axonframework.common.jpa.ContainerManagedEntityManagerProvider;
import org.axonframework.common.jpa.EntityManagerProvider;
import org.axonframework.common.transaction.TransactionManager;
import org.axonframework.eventhandling.EventBus;
import org.axonframework.eventhandling.EventMessage;
import org.axonframework.eventsourcing.*;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.axonframework.eventsourcing.eventstore.jpa.JpaEventStorageEngine;
import org.axonframework.messaging.annotation.ParameterResolverFactory;
import org.axonframework.messaging.interceptors.TransactionManagingInterceptor;
import org.axonframework.spring.config.EnableAxon;
import org.axonframework.spring.eventsourcing.SpringAggregateSnapshotter;
import org.axonframework.spring.eventsourcing.SpringAggregateSnapshotterFactoryBean;
import org.axonframework.spring.eventsourcing.SpringPrototypeAggregateFactory;
import org.hibernate.event.spi.AbstractEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import learn.deb.ms.booksmanagement.entities.BookMeta;

import learn.deb.ms.booksmanagement.model.Book;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Configuration
public class AxonConfig {

	@Autowired
	private EventStore eventStore;
	
	@Bean
	@Scope("prototype")
	public BookMeta bookMeta() {
		return new BookMeta();
	}

	@Bean
	@Scope("prototype")
	public Book book() {
		return new Book();
	}
	
	@Bean
	public AggregateFactory<Book> bookAggregateFactory() {
		SpringPrototypeAggregateFactory<Book> aggregateFactory = new SpringPrototypeAggregateFactory<>();
		aggregateFactory.setPrototypeBeanName("book");

		return aggregateFactory;
	}

	@Bean
	public AggregateFactory<BookMeta> bookMetaAggregateFactory() {
		SpringPrototypeAggregateFactory<BookMeta> aggregateFactory = new SpringPrototypeAggregateFactory<>();
		aggregateFactory.setPrototypeBeanName("bookMeta");

		return aggregateFactory;
	}

	//*********************This is for JPA Repo*************Comment the EventBus in handler
	/*@Bean
	public Repository<BookMeta> bmRepository(EventBus eventBus) {
		GenericJpaRepository<BookMeta> repository = new GenericJpaRepository<BookMeta>(entityManagerProvider(), BookMeta.class, eventBus);
		return repository;
	}

	@Autowired
	private TransactionManager transactionManager;

	@PersistenceContext
	private EntityManager entityManager;

	@Bean
	public EntityManagerProvider entityManagerProvider() {
		return new ContainerManagedEntityManagerProvider();
	}

	@Bean
	public JpaEventStorageEngine eventStorageEngine() {
		ContainerManagedEntityManagerProvider ctnmp = new ContainerManagedEntityManagerProvider();
		ctnmp.setEntityManager(entityManager);
		return new JpaEventStorageEngine(ctnmp, transactionManager);
	}*/
	//*********************This is for JPA Repo*************Comment the EventBus in handler

	/*/*//* - Only EventSourcing ################################################################################*/
	@Bean
	public Repository<BookMeta> bmRepository() {
		EventSourcingRepository<BookMeta> repository = new EventSourcingRepository<>(bookMetaAggregateFactory(),
				eventStore);
		return repository;
	}
	//Only EventSourcing #######################################################################################

	// EventSourcing With Snapshot after 3 instances ---------------------------------------------------
	/*@Bean
	public Repository<BookMeta> bmRepository(SnapshotTriggerDefinition snapshotTriggerDefinition) {
		CachingEventSourcingRepository<BookMeta> repository = new CachingEventSourcingRepository<>(bookMetaAggregateFactory(),eventStore,cache(),snapshotTriggerDefinition);
		return repository;
	}

	@Bean
	public SpringAggregateSnapshotterFactoryBean  snapshotter() {
		return new SpringAggregateSnapshotterFactoryBean();
	}

	@Bean
	public SnapshotTriggerDefinition snapshotTriggerDefinition() throws Exception {
		return new EventCountSnapshotTriggerDefinition(snapshotter().getObject(), 3);
	}

	@Bean
	public Cache cache(){
		return new WeakReferenceCache();
	}*/
	// With Snapshot after 3 instances -------------------------------------------------------------------

	@Bean
	public Repository<Book> libraryBook() {
		EventSourcingRepository<Book> repositoryBook = new EventSourcingRepository<Book>(bookAggregateFactory(),
				eventStore);
		return repositoryBook;
	}
	
	@Bean
	public TransactionManagingInterceptor<EventMessage<AbstractEvent>> transactionManagingInterceptor(TransactionManager axonTransactionManager){
		return new TransactionManagingInterceptor<EventMessage<AbstractEvent>>(axonTransactionManager);
	}
	
}
