package learn.deb.ms.booksmanagement.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import learn.deb.ms.booksmanagement.coreapi.BookMetaUpdatedEvent;
import lombok.*;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.commandhandling.model.AggregateRoot;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

import learn.deb.ms.booksmanagement.coreapi.BookMetaCreatedEvent;

@Aggregate
@Entity
@Table(name="book_meta")
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookMeta {
	
	@AggregateIdentifier
	@Id
	private String metaid;
	
	@Basic
	private String name;
	
	@Basic
	private String author;
	
	@Basic
	private String genre;
	
	/*public BookMeta(String name, String author, String genre) {
		this.name = name;
		this.author = author;
		this.genre = genre;
	}*/
	
	@EventSourcingHandler
	public void on(BookMetaCreatedEvent event){

		this.setMetaid(event.getMetaid());
		this.setName(event.getName());
		this.setAuthor(event.getAuthor());
		this.setGenre(event.getGenre());

		System.out.println("BookMeta -> "+event.getMetaid()+" created successfully");
	}


	@EventSourcingHandler
	public void on(BookMetaUpdatedEvent event){
		this.setMetaid(event.getMetaid());
		this.setName(event.getName());
		this.setAuthor(event.getAuthor());
		this.setGenre(event.getGenre());

		System.out.println("BookMeta -> "+event.getMetaid()+" updated successfully");
	}
}
